# Academico22

Sistema de controle acadêmico desenvolvido em Laravel conjuntamente com os estudantes da segunda série do Curso de Automação Industrial - CodaAut - IFMG - OP

Este projeto tem o objetivo de auxiliar os estudantes no entendimento do Laravel e do paradigma MVC, além do aprofundamento em HTML, CSS e JavaScript.


## Configuracões:

Após clonar a aplicação do GitLab, você deverá executar (no terminal do VsCode) a sequência de comandos abaixo:

### Configurar o git

git config --global user.name "seu_nome_de_usuario_no_gitlab"

git config --global user.email "seu_email_de_usuario_no_gitlab"


### Criar o banco de dados

touch database/database.sqlite

### Atualizar o Composer

composer update

### Efetuar a migração das tabelas e popular o banco de dados

php artisan migrate:fresh

php artisan db:seed

### subir um servidor na máquina local

php artisan serve
