@section('conteudo')
  <div class="w-auto container_login">
    <table class="table table-striped ">
      <thead class="thead-dark">
        <tr>
          <th>Nome</th>
          <th>Matrícula</th>
          <th>Tipo</th>
          <th>Cpf</th>
          <th>Email</th>
          <th>Telefone</th>
          <th>Senha</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($user as $user)
        <tr>
          <td> {{ $user->nome}}</td>
          <td> {{ $user->matricula}}</td>
          <td> {{ $user->tipo}} </td>
          <td> {{ $user->cpf}} </td>
          <td> {{ $user->email}} </td>
          <td> {{ $user->telefone}} </td>
          <td> {{ $user->senha}} </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  @endsection